package main

// swagger:model
type Parameter struct {
	Comuna 		string 		`json:"comuna"`
	Local 			string 		`json:"local"`
}

// swagger:model
type Response struct {
	Error 			bool 		`json:"error"`
	Code_Error 		string 		`json:"code_error"`
	Description 	string		`json:"description"`
	Data 			[]PharmacyResponse  `json:"data"`
}

// swagger:model
type Pharmacy struct {
	Name 			string 		`json:"local_nombre"`
	Commune			string		`json:"comuna_nombre"`
	Address 		string 		`json:"local_direccion"`
	Phone 			string		`json:"local_telefono"`
	Latitude 		string		`json:"local_lat"`
	Longitude 		string		`json:"local_lng"`
}

// swagger:model
type PharmacyResponse struct {
	Name 			string 		`json:"local_nombre"`
	Address 		string 		`json:"local_direccion"`
	Phone 			string		`json:"local_telefono"`
	Latitude 		float64		`json:"local_lat"`
	Longitude 		float64		`json:"local_lng"`
}