API Farmacias de turno en la Region Metropolitana de Chile

Esta API esta desarrollada en Go version go1.11.6 linux/amd64

Estructura del API

main.go
- Definicion del end point /api/pharmacy e instancia del Server en el puerto 8888

get_pharmacy.go
- Recibe la solicutud http, la procesa y escribe la respuesta al cliente

structures.go
- Definicion de los modelos a manejas en el API

tools.go
- Funciones generales para escribir en los log y relizar un http Get

config.go
- Archivo de configuracion del API

swagger.json
- Documentacion del API

farmacia
- Binario ejecutable para la arquitectura de Linux

farmacia.exe
- Ejecutable para la arquitectura de Windows


Ejecutar Dockerfile

Construir Docker
- `docker build -t api-farmacia .`

Inicar Docker
- `docker run --publish 8888:8888 -t api-farmacia`

Detener Docker
- `docker stop <CONTAINER ID>`
