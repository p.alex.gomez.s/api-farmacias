package main

import (
	"net/http"
	"encoding/json"
	"strings"
	"strconv"
)

func PharmacySearch(w http.ResponseWriter, r *http.Request) {
    // swagger:operation GET /api/pharmacy PharmacySearch
    //
    // Retorna las farmacias de turno disponibles en la Region Metropolitana de Santiago
    // ---
    // consumes:
    // - multipart/form-data
    // produces:
    // - application/json
    // parameters:
    // - name: comuna
    //   in: query
    //   description: Nombre de la comuna donde se buscara la farmacia de turno.
    //   required: true
	//   type: string
	// - name: local
    //   in: query
    //   description: Nombre del local de turno a buscar.
    //   required: true
    //   type: string
    // responses:
    //   '200':
    //     description: Estructura resultante de la petición al servicio.
	//     type: json
	setupCORS(&w,r)
	keys := r.URL.Query()
	var response Response
	var findPharmacy []PharmacyResponse
	commune, issetCommune:= keys["comuna"]
	local, issetLocal := keys["local"]
	if issetCommune && issetLocal {
		data, codeError := Get(urlAPI)
		if codeError == "00" {
			for d := range data {
				if strings.ToUpper(data[d].Commune) == strings.ToUpper(commune[0]) && strings.ToUpper(data[d].Name) == strings.ToUpper(local[0]) {
					lat, _ := strconv.ParseFloat(data[d].Latitude,64)
					lng, _ := strconv.ParseFloat( data[d].Longitude,64)
					findPharmacy = append(findPharmacy, PharmacyResponse{Name: data[d].Name, Address: data[d].Address, Phone: data[d].Phone, Latitude: lat, Longitude: lng})
				}
			}
			if findPharmacy != nil {
				response = ResponseFunc(false, "00", "Datos encontrados")
				response.Data = findPharmacy
			}else{
				response = ResponseFunc(false, "00", "No se encontraron datos")
			}
		}else{
			response = ResponseFunc(true, codeError, "Error conectando al API farmanet.minsal.cl")
		}
		
	}else{
		response = ResponseFunc(true, "01", "Parametros invalidos")
	}
	responseJson, _ := json.Marshal(response)
	w.Write([]byte(responseJson))
}

