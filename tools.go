package main

import (
	"net/http"
	"log"
	"encoding/json"
	"io/ioutil"
	"strconv"
)

func ResponseFunc(err bool, errorCode string, description string) Response {
	var response Response
	response.Error = err
	response.Code_Error = errorCode
	response.Description = description
	log.Println("Response [ Error: " + strconv.FormatBool(err) + " - Error Code: " + errorCode + " - Description: " + description + "]")

	return response
}

func Get(url string) ([]Pharmacy, string) {
	var result []Pharmacy
	var codeError = "00"
	resp, err := http.Get(url)
	if err != nil {
		codeError = "02"
	}else{
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		err = json.Unmarshal(body,&result)
	}

	return result, codeError
}

func setupCORS(w *http.ResponseWriter, req *http.Request) {
    (*w).Header().Set("Access-Control-Allow-Origin", "*")
    (*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
