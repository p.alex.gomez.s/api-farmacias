// Testing go-swagger generation
//
// The purpose of this application is to test go-swagger in a simple GET request.
//
//     Schemes: http
//     Host: localhost:8888
//     Version: 0.0.1
//     Contact: Pedro Gomez<p.alex.gomez.s@gmail.com>
//
//     Consumes:
//     - multipart/form-data
//
//     Produces:
//     - application/json
//
// swagger:meta
package main

import (
	"net/http"
	"log"
)

func main() {
	http.HandleFunc("/api/pharmacy", PharmacySearch)
	http.HandleFunc("/swagger.json", swagger)
	log.Printf("Pharmacy Service run in port 8888")
	if err := http.ListenAndServe(":8888", nil); err != nil {
	  panic(err)
	}
  }


  func swagger(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    http.ServeFile(w, r, "swagger.json")
}